# Kajnimoto tactile display drivers

This project contains Kajinomoto tactile display drivers

# Dependencies

* *cmake 3.9+*
* *GCC 4.8+* or any other compiler supporting C++11

# Build

```
mkdir build && cd build
cmake ..
make
```
