#pragma once

#include "finger_pad.h"

#include <vector>
#include <string>

namespace kajinomoto
{
  
class TactileDisplay {
public:
    void setVolume(uint8_t volume);
    inline uint8_t volume() { return volume_; }

    void setScaling(uint8_t scaling);
    inline uint8_t scaling() { return scaling_; }

    FingerPad & fingerPad(const std::string &id);
    void updateFingerPad(const std::string &id);

private:
    uint8_t volume_;
    uint8_t scaling_;
    std::vector<FingerPad> fingerPads_;
};

}
