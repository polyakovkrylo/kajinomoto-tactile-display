#pragma once

#include <string>
#include <array>

namespace kajinomoto {

class TactileDisplay;

class FingerPad
{
    template <int rows, int columns, typename T>
    using matrix = std::array<std::array<T, columns>, rows>;

public:
    static constexpr int rows {4};
    static constexpr int columns {5};

    FingerPad(const std::string &id);

    void setPad(unsigned long column, unsigned long row, uint8_t value);
    void setColumn(unsigned long index, uint8_t value);
    void setRow(unsigned long index, uint8_t value);

    inline int pad(unsigned long row, unsigned long column) const { return pads_[row][column]; }
    inline const matrix<rows, columns, uint8_t> & pads() const { return pads_; }

    inline const std::string & id() const { return id_; }

private:
    uint8_t masterValue_;
    matrix<rows, columns, uint8_t> pads_;
    std::string id_;

    TactileDisplay *display_;
};

}
