cmake_minimum_required(VERSION 3.9)

include(GNUInstallDirs)

project(KajinomotoTactileDisplay
  VERSION 0.1
  DESCRIPTION "Kajinomoto tactile display driver"
  )

add_library(KajinomotoTactileDisplay SHARED
  src/tactile_display.cpp
  src/finger_pad.cpp
  src/communication_port.cpp
  )

set_target_properties(KajinomotoTactileDisplay PROPERTIES
  VERSION ${PROJECT_VERSION}
  )

set_target_properties(KajinomotoTactileDisplay PROPERTIES
  PUBLIC_HEADER FILES
  include/tactile_display.h
  include/finger_pad.h
  )

target_include_directories(KajinomotoTactileDisplay PRIVATE include)
target_include_directories(KajinomotoTactileDisplay PRIVATE src)

install(TARGETS KajinomotoTactileDisplay
  LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
  PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}
  )
