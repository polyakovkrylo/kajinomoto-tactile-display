#include "finger_pad.h"
#include "tactile_display.h"

namespace kajinomoto {
using std::string;

FingerPad::FingerPad(const string &id) :
    id_(id)
{

}

void FingerPad::setPad(unsigned long column, unsigned long row, uint8_t value)
{
    pads_[row][column] = value;
    display_->updateFingerPad(id_);
}


void FingerPad::setColumn(unsigned long index, uint8_t value)
{
    for ( auto &row : pads_ ) {
        row[index] = value;
    }
    display_->updateFingerPad(id_);
}

void FingerPad::setRow(unsigned long index, uint8_t value)
{
    for ( auto &element : pads_[index]) {
        element = value;
    }
    display_->updateFingerPad(id_);
}

}
