#ifndef COMMUNICATIONPORT_H
#define COMMUNICATIONPORT_H

#include <string>
#include <memory>

namespace kajinomoto {

class IODevice;

class CommunicationPort
{
public:
    CommunicationPort() = default;
    CommunicationPort(const std::string &portName);

    void send(uint8_t topic, uint8_t *data, size_t length);

private:
    std::unique_ptr<IODevice> device_;
    constexpr uint8_t stxByte {0x33};

    uint8_t checksum(uint8_t *data, size_t length);
};

}

#endif // COMMUNICATIONPORT_H
