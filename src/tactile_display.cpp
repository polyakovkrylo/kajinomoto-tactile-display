#include "tactile_display.h"

namespace kajinomoto {

using std::string;

void TactileDisplay::setVolume(uint8_t volume) {
    volume_ = volume;
}

void TactileDisplay::setScaling(uint8_t scaling) {
    scaling_ = scaling;
}

FingerPad & TactileDisplay::fingerPad(const string &id) {
    auto it = fingerPads_.begin();

    while (it != fingerPads_.end()) {
        if (it->id() == id)
            break;
    }

    return *it;
}

void TactileDisplay::updateFingerPad(const string &id) {
    // send a message via the communication port
}


}
