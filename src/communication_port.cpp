#include "communication_port.h"

#include <vector>

namespace kajinomoto {

using std::string;
using std::vector;

CommunicationPort::CommunicationPort(const string &portName)
{
    (void)portName;
}

void kajinomoto::CommunicationPort::send(uint8_t topic, uint8_t *data, size_t length)
{
    constexpr size_t headerSize = sizeof(stxByte) + sizeof(topic);
    constexpr size_t checksumSize = sizeof(uint8_t);

    vector<uint8_t> msg;
    msg.reserve(headerSize + length + checksumSize);

    msg.push_back(stxByte);
    msg.push_back(topic);

    for (size_t i = 0; i < length ; i++) {
        msg.push_back(data[i]);
    }

    msg.push_back(checksum(msg.data(), msg.size()));

    //    device_->write(msg);
}

uint8_t CommunicationPort::checksum(uint8_t *data, size_t length)
{
    uint8_t sum;
    for (size_t i = 0; i < length; i++) {
        sum += data[i];
    }
    return ~sum;
}

}
